# KaMii <lacielacie@rocketmail.com>

# PirateLeft 2019
# All wrongs reserved

# I dont give a flying fuck what you do with this script.  Hack it, sell it,
# compile it, delete it, fap to it.

# 1. Redistribution consequences:

# YOU MUST GIVE CREDIT TO THE ORIGINAL AUTHOR OF THIS SCRIPT WITH A COPY OF THIS
# MESSAGE.  USING THIS SCRIPT IS YOUR BUSINESS.  NO ONE IS FORCING YOU TO USE IT,
# SO IF YOUR COMPUTER BLOWS UP AND EVERYONE ON PLANET EARTH IS KILLED IN THE
# PROCESS, DO NOT BLAME ANYONE BUT YOURSELF FOR BEING TOO STUPID TO RTFM BEFORE
# EXECUTING THIS SCRIPT.  I WILL STOP WRITING DISCLAIMER INFORMATION NOW IN
# CAPITAL LETTERS BECAUSE I KNOW NO ONE EVEN READS THIS SHIT ANYMORE.

EAPI="5"

inherit eutils waf-utils python-single-r1

DESCRIPTION="Thunar Dropbox is a plugin for thunar that adds context-menu items from dropbox."
HOMEPAGE="http://softwarebakery.com/maato/${PN}.html"
SRC_URI="http://nullwise.com/files/${PN}/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="${PYTHON_DEPS}
    dev-python/bar[${PYTHON_USEDEP}]
    dev-python/foo[${PYTHON_USEDEP}]"
DEPEND="${PYTHON_DEPS}
    dev-python/frobnicate
    test? ( ${RDEPEND} )"

DEPEND="net-misc/dropbox"
RDEPEND="${DEPEND}"

src_unpack() {
    unpack ${A}
    cd "${S}"

}

src_configure() {
    waf-utils_src_configure
}

src_compile() {
    waf-utils_src_compile
}

src_install() {
    waf-utils_src_install
}

pkg_postinst() {
	gnome2_icon_cache_update
	xdg_desktop_database_update
}

pkg_postrm() {
	gnome2_icon_cache_update
	xdg_desktop_database_update
}