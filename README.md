# Gentoo
These are my personal ebuild scripts for Gentoo

## License
I do not use the "GNU General Public License v2" like Gentoo ships with all their ebuilds.  Please have a look at my license before using my scripts.

## Adding my overlay
layman -f -o https://gitlab.com/KaMiila/KaMii-Builds/blob/master/repositories.xml -a KaMii-Builds

## It's a trap!
I like compiling things directly from git, tarballs piss me off!  If my ebuild has the version number 9999 that means it will pull from the master branch.  Otherwise if it has a real version number (seriously would be funny if I can find an application whos version number actually is 9999) then it will most likely pull from the git branch who's tag matches the version number (I say most likely because there is the random possiblity I will tell it to grab a tarball).  Make sense?

## Bugs
If you find a bug or issue please open a report or email me directly at lacielacie@rocketmail.com

## Warning
USE OF THESE SCRIPTS ARE "AS IS" AND SHOULD NEVER BE USED BY ANYONE!  THIS MEANS YOU!  FAILURE TO READ AND UNDERSTAND ANY SCRIPT BEFORE EXECUTING MAY RESULT IN TOTAL GLOBAL DESTRUCTION ON AN EPIC SCALE.  SUCH EXTINCTION LEVEL EVENTS (ELE) CAN BE AVOIDED IF YOU TAKE THE TIME TO RTFM.  IF, AFTER EXECUTING ANY CODE FOUND ON THE INTERNET WHICH IS NOT YOURS, CAUSES YOUR COMPUTER TO EVOLVE INTO A NEW LIFE FORM WHICH DECIDES TO ELIMINATE ALL HUMANS... THE BLAME IS COMPLETELY ON YOUR HANDS NOT THE AUTHOR(S) OF SAID CODE.  ALL SCRIPTS WILL INCLUDE A README FILE. IF YOU FAIL TO READ AND COMPREHEND THIS FILE BEFORE EXECUTING THE CODE, YOUR COMPUTER MAY START A CASCADING EVENT WHICH WILL OPEN UP A BLACK HOLE CAUSING OUR ENTIRE UNIVERSE TO DISAPPEAR. IF YOU DOWNLOADED A SCRIPT AND HAVE NO IDEA WHAT IT DOES AFTER READING BOTH THE README FILE AND THE SCRIPT ITSELF, EXECUTE A DOD 5220.22-M SECURE DELETE OF THE FILE OR A GUTMANN METHOD SO OUR UNIVERSE WILL NOT SPONTANEOUSLY EXPLODE.  REMEMBER, THE NSA IS ALWAYS WATCHING YOU, EVEN WHEN YOU SLEEP.

. . .

JUST LIKE SANTA CLAUS.