# KaMii <lacielacie@rocketmail.com>

# PirateLeft 2019
# All wrongs reserved

# I dont give a flying fuck what you do with this script.  Hack it, sell it,
# compile it, delete it, fap to it.

# 1. Redistribution consequences:

# YOU MUST GIVE CREDIT TO THE ORIGINAL AUTHOR OF THIS SCRIPT WITH A COPY OF THIS
# MESSAGE.  USING THIS SCRIPT IS YOUR BUSINESS.  NO ONE IS FORCING YOU TO USE IT,
# SO IF YOUR COMPUTER BLOWS UP AND EVERYONE ON PLANET EARTH IS KILLED IN THE
# PROCESS, DO NOT BLAME ANYONE BUT YOURSELF FOR BEING TOO STUPID TO RTFM BEFORE
# EXECUTING THIS SCRIPT.  I WILL STOP WRITING DISCLAIMER INFORMATION NOW IN
# CAPITAL LETTERS BECAUSE I KNOW NO ONE EVEN READS THIS SHIT ANYMORE.

EAPI=5

inherit qmake-utils games gnome2-utils git-r3 xdg-utils

DESCRIPTION="Map editor for the Endless Sky universe."
HOMEPAGE="https://github.com/endless-sky/endless-sky-editor"

EGIT_REPO_URI="https://github.com/endless-sky/endless-sky-editor"

LICENSE="GPL-3"
SLOT="0"
IUSE=""

RDEPEND="dev-qt/qtcore:5"

DEPEND="${RDEPEND}
		virtual/pkgconfig
		dev-vcs/git"

DOCS=( LICENSE README.md )


# Adding patch so this installs to the correct location

src_prepare () {
	epatch \
			"${FILESDIR}"/${PN}.patch	
}

src_configure() {
	local myqmakeargs=(
		PREFIX="${EPREFIX%/}/usr"
		SYSTEMQTSA=1
	)
	eqmake5 "${myqmakeargs[@]}"
}

src_install() {
	emake INSTALL_ROOT="${D}" install
	einstalldocs
}

pkg_postinst() {
	gnome2_icon_cache_update
	xdg_desktop_database_update
}

pkg_postrm() {
	gnome2_icon_cache_update
	xdg_desktop_database_update
}